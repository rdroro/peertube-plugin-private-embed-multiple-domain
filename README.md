# PeerTube plugin private embedding for multiple domains

This plugin is a fork from [peertube-plugin-private-embed](https://github.com/osiris86/peertube-plugin-private-embed). It support multiple domains for restricted embed videos.

A pull request to the original repository could be proposed.
