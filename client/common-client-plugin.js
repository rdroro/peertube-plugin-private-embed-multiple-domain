async function register({ registerHook, peertubeHelpers, registerVideoField }) {
  registerHook({
    target: 'filter:api.video-watch.video.get.result',
    handler: async (result, params) => {
      const api =
        peertubeHelpers.getBaseRouterRoute() + '/private-embed/' + result.id

      const res = await fetch(api, {
        method: 'GET',
        headers: peertubeHelpers.getAuthHeader()
      })

      const privateResponse = await res.json()

      if (privateResponse.embedOnly) {
        result.streamingPlaylists = []
        result.name = ''
      }
      return result
    }
  })

  const isNullOrEmpty = (value) => {
    return !value || value.length === 0
  };

  const isDomainStartByProtocol = (domain) => {
    return domain.search(/^https?:\/\//g) !== -1
  }
  
  const isDomainEndWithSlash = (domain) => {
    return domain.substring(domain.length - 1) === '/'
  }

  const SEPARATED_DOMAIN_CHARACTER = ","

  const restrictEmbeddingLabel = await peertubeHelpers.translate(
    'restrict-embedding-label'
  )
  const restrictEmbeddingDomainLabel = await peertubeHelpers.translate(
    'restrict-embedding-domain-label'
  )
  const restrictEmbeddingDomainDescription = await peertubeHelpers.translate(
    'restrict-embedding-domain-description'
  )

  const checkboxOptions = {
    name: 'restrict-embedding',
    label: restrictEmbeddingLabel,
    type: 'input-checkbox',
    default: false,
    hidden: ({ formValues, videoToUpdate, liveVideo }) => {
      return formValues['privacy'] !== 2
    }
  }

  const domainOptions = {
    name: 'restrict-embedding-domain',
    label: restrictEmbeddingDomainLabel,
    descriptionHTML: restrictEmbeddingDomainDescription,
    type: 'input',
    default: '',
    hidden: ({ formValues, videoToUpdate, liveVideo }) => {
      if (formValues.privacy !== 2) {
        return true
      }

      return !(
        formValues.pluginData && formValues.pluginData['restrict-embedding']
      )
    },

    error: async ({ formValues, value }) => {
      const restrictEmbedding = formValues.pluginData['restrict-embedding']
      if (!restrictEmbedding) {
        return { error: false }
      }
      const currentValue = formValues.pluginData['restrict-embedding-domain']
      if (isNullOrEmpty(currentValue)) {
        const restrictEmbeddingErrorNotEmpty = await peertubeHelpers.translate(
          'restrict-embedding-error-not-empty'
        )
        return {
          error: true,
          text: restrictEmbeddingErrorNotEmpty
        }
      }
        console.log(typeof currentValue);
      const currentValueSplited = typeof currentValue === "string" ? currentValue.split(SEPARATED_DOMAIN_CHARACTER) : [];
      for (let i=0; i < currentValueSplited.length; i++) {
          const domain = currentValueSplited[i];
          if(!isDomainStartByProtocol(domain)) {
            const restrictEmbeddingErrorProtocol = await peertubeHelpers.translate(
              'restrict-embedding-error-protocol'
            )
            return {
              error: true,
              text: restrictEmbeddingErrorProtocol
            }
          }

          if (!isDomainEndWithSlash(domain)) {
            const restrictEmbeddingErrorSlash = await peertubeHelpers.translate(
              'restrict-embedding-error-slash'
            )
            return { error: true, text: restrictEmbeddingErrorSlash }
          }
      }
    }
  }

  const videoFormOptions = {
    tab: 'main'
  }

  for (const type of [
    'upload',
    'import-url',
    'import-torrent',
    'update',
    'go-live'
  ]) {
    registerVideoField(checkboxOptions, { type, ...videoFormOptions })
    registerVideoField(domainOptions, { type, ...videoFormOptions })
  }
}

export { register }
